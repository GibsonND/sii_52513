#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>


int main(int argc, char *argv[])
{
	int fd;
	int v[2];
	/* crea el tuberiaFIFO */
	if (mkfifo("/tmp/tuberiaFIFO", 0600)<0) {
		perror("No puede crearse el tuberiaFIFO");
		return(1);
	}
	/* Abre el tuberiaFIFO */
	if ((fd=open("/tmp/tuberiaFIFO", O_RDONLY))<0) {
		perror("No puede abrirse el tuberiaFIFO");
		return(1);
	}
	while (read(fd, &v, sizeof(v))==sizeof(v)) 
	{
		printf("Jugador %d ha marcado, lleva un total de %d puntos\n",v[0], v[1]);		
	}

	close(fd);
	unlink("/tmp/tuberiaFIFO");
	return(0);
} 
	
