// Esfera.cpp: implementation of the Esfera class.
//
//////////////////////////////////////////////////////////////////////

#include "Esfera.h"
#include "glut.h"
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Esfera::Esfera()
{
	radio=(0.5f); //MIRAR 2
	velocidad.x=3;
	velocidad.y=3;
}

Esfera::~Esfera()
{

}

float Esfera::n=10; 


void Esfera::Dibuja()
{
	glColor3ub(255+n,255-n,0); 
	glEnable(GL_LIGHTING);
	n+=10;
	glPushMatrix();
	glTranslatef(centro.x,centro.y,0);
	if(n>1000)
	{	
		n=10;
	}
	glutSolidSphere(radio-(n/1000),15,15);
	glPopMatrix();
}

void Esfera::Mueve(float t) //comprobar
{
        centro=centro+velocidad*t;
}
